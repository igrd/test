﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvc5testapp.Models;
using System.Data.Entity;

namespace mvc5testapp.Controllers
{
    public class HomeController : Controller
    {
        private TestContext db = new TestContext();

        [HttpGet]
        public ActionResult Index(int page = 1, string sort = "name", string sortdir = "asc", int psize = 0)
        {
            IndexViewModel indexViewModel;
            List<User> users = db.Users.ToList();
            db.Statuses.Load();
            indexViewModel = new IndexViewModel();
            
            indexViewModel.Pagination.CurrenPageNumber = page;
            indexViewModel.SortInfo.OrderBy = sort;
            indexViewModel.SortInfo.OrderDirection = sortdir;
            indexViewModel.Pagination.PageSize = indexViewModel.PageSizes.FirstOrDefault(x => x.Id == psize);
            if(indexViewModel.Pagination.PageSize == null)
                indexViewModel.Pagination.PageSize = indexViewModel.PageSizes.First();
            
            indexViewModel.Pagination.PagesCount = (int)Math.Ceiling((decimal)users.Count / indexViewModel.Pagination.PageSize.Size);

            users = this.SortUsers(users, indexViewModel.SortInfo);
            //pagination:
            indexViewModel.Users = users.Skip((indexViewModel.Pagination.CurrenPageNumber - 1) * indexViewModel.Pagination.PageSize.Size).Take(indexViewModel.Pagination.PageSize.Size).ToList();
            
            return View(indexViewModel);
        }


        private List<User> SortUsers(List<User> users, SortInfo sortInfo)
        {
            Func<User, String> GetOrderKey = (u) => u.Name;

            switch (sortInfo.OrderBy)
            {
                case "status":
                    GetOrderKey = (u) => u.StatusId.ToString();
                    break;
                case "regdate":
                    GetOrderKey = (u) => u.RegDate.ToString();
                    break;
                default:
                    GetOrderKey = (u) => u.Name.ToString();
                    break;
            }

            if (sortInfo.OrderDirection == "desc")
            {
                users = users.OrderByDescending<User, String>(GetOrderKey).ToList();
                sortInfo.OrderDirection = "desc";
            }
            else
            {
                users = users.OrderBy<User, String>(GetOrderKey).ToList();
                sortInfo.OrderDirection = "asc";
            }
            return users;
        }
       
        [HttpPost] 
        public ActionResult Index()
        {
            return RedirectToAction("Index", new { psize = Request.Form["Pagination.PageSize.Id"] });
        }
        

        [HttpGet]
        public ActionResult UserInfo(int? Id)
        {
            if (Id == null)
                return HttpNotFound();
            UserinfoViewModel userViewModel = new UserinfoViewModel();
           
            userViewModel.User = db.Users.ToList().Find(u => u.Id == Id);
            if(userViewModel.User==null)
                return HttpNotFound();
            
            userViewModel.Statuses = db.Statuses.ToList();
            userViewModel.Roles = db.Roles.ToList();
            
            return View("UserInfo", userViewModel);
        }


        [HttpPost]
        public ActionResult UserInfo(User user, int[] selectedRoles)
        {
            if(!ModelState.IsValid)
            {
                TempData["Message"] = "Данные введены некорректно.";
                return RedirectToAction("UserInfo", new { Id = user.Id });
            }
            
            if(selectedRoles==null)
            {
                TempData["Message"] = "Пользователь должен иметь минимум одну роль.";
                return RedirectToAction("UserInfo", new { Id = user.Id });
            }
            
            User updUser = db.Users.Find(user.Id);
            
            if (updUser.LastUpdateTime != user.LastUpdateTime)
            {
                TempData["message"] = "Данная запись уже была кем-то изменена. Попробуйте еще раз.";
                return RedirectToAction("UserInfo", new {id = user.Id });
            }
            
            updUser.Name = user.Name;
            updUser.phone = user.phone;
            updUser.RegDate = user.RegDate;
            updUser.StatusId = user.StatusId;
            updUser.BirthDate = user.BirthDate;
            updUser.email = user.email;
            updUser.phone = user.phone;
            //все кроме миллисекунд (т.к. в базе они сохраняются, а при передаче через форму - теряются):
            updUser.LastUpdateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            updUser.Roles.Clear();
            if (selectedRoles != null)
            {
                foreach (var r in db.Roles.Where(ro => selectedRoles.Contains(ro.Id)))
                {
                    updUser.Roles.Add(r);
                }
            }

            db.Entry(updUser).State = EntityState.Modified;
            db.SaveChanges();
            
            return RedirectToAction("Index");
        }
    }
}