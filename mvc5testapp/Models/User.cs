﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace mvc5testapp.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Дата регистрации")]
        public DateTime RegDate { get; set; }

        [Required]
        [Display(Name = "Статус")]
        public int StatusId { get; set; }

        [Required]
        [Display(Name = "Дата рождения")]
        public DateTime BirthDate { get; set; }
        
        [Display(Name = "email")]
        [EmailAddress]
        public string email { get; set; }

        [Display(Name = "Телефон")]
        [Phone]
        public string phone { get; set; }

        public DateTime LastUpdateTime { get; set; }
        
        public Status Status { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public User()
        {
            Roles = new List<Role>();
        }
    }
}
