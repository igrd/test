﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc5testapp.Models
{
    public class UserinfoViewModel
    {
        public User User {get;set;}
        public List<Status> Statuses { get; set; }
        public List<Role> Roles { get; set; }
        
        public UserinfoViewModel()
        {
            Statuses = new List<Status>();
            Roles = new List<Role>();
        }
    }
}