﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc5testapp.Models
{
    public class IndexViewModel
    {
        public List<User> Users { get; set; }
        public Pagination Pagination { get; set; }
        public SortInfo SortInfo { get; set; }
        public List<PageSize> PageSizes { get; set; }

        public IndexViewModel()
        { 
            Users = new List<User>();
            Pagination = new Pagination();
            SortInfo = new SortInfo();
            PageSizes = new List<PageSize>();

            PageSizes.Add(new PageSize(0, 10));
            PageSizes.Add(new PageSize(1, 20));
            PageSizes.Add(new PageSize(2, 50));
            PageSizes.Add(new PageSize(3, 100));
        }
    }
}