﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace mvc5testapp.Models
{
    public class Role
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        
        public virtual ICollection<User> Users {get; set;}
         
        public Role()
        {
            Users = new List<User>();
        }
    }
}