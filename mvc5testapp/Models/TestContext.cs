﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace mvc5testapp.Models
{
    public class TestContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Statuses { get; set; }
        
        public TestContext()
        {
            //раскомментировать для заполения БД при первом запуске
            //Database.SetInitializer<TestContext>(new TestContextInitializer());
        }
    }

    public class TestContextInitializer: DropCreateDatabaseAlways<TestContext>
    {
        protected override void Seed(TestContext db)
        {
            Status s1 = new Status { Name = "Active"};
            Status s2 = new Status { Name = "Blocked" };
            db.Statuses.Add(s1);
            db.Statuses.Add(s2);
            db.SaveChanges();

            Role r1 = new Role{Name = "Admin"};
            Role r2 = new Role { Name = "User" };
            Role r3 = new Role { Name = "Supervisor" };
            db.Roles.Add(r1);
            db.Roles.Add(r2);
            db.Roles.Add(r3);
            db.SaveChanges();
            
            User u1;
            for(int i = 0; i<100; i++)
            {
                DateTime now = DateTime.Now;
                //все кроме миллисекунд:
                now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
                    
                u1 = new User { Name = "user"+i.ToString(), RegDate = DateTime.Parse("11.11.2000"), BirthDate = DateTime.Parse("01.01.1900"), StatusId = (new Random(i).Next(1,3)), phone = "2221144", email = "wer@eee.dd", LastUpdateTime = now };

                u1.Roles.Add(r2);
                if(new Random(i).Next(1,4)>1)
                    u1.Roles.Add(r1);
                if(new Random(i).Next(1,4)>2)
                    u1.Roles.Add(r3);
                db.Users.Add(u1);
            }

            db.SaveChanges();

        }
    }
}