﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mvc5testapp.Models
{
    public class SortInfo
    {
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string InvertedOrderDirection 
        { 
            get
            {
                if (OrderDirection == "asc" || OrderDirection == "")
                    return "desc";
                else
                    return "asc";
            }    
        }
    }
}
