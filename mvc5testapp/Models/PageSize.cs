﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc5testapp.Models
{
    public class PageSize
    {
        public int Id { get; set; }
        public int Size { get; set; }
        public PageSize(int id, int size)
        {
            Id = id;
            Size = size;
        }
    }
}