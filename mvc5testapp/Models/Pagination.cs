﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mvc5testapp.Models
{
    public class Pagination
    {
        public int CurrenPageNumber { get; set; }
        public PageSize PageSize { get; set; }
        public int PagesCount { get; set; }
    }
}
