﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace mvc5testapp.Models
{
    public class Status
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        
        public ICollection<User> Users {get; set;}

        public Status()
        {
            Users = new List<User>();
        }
        
    }
}